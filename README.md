# OpenML dataset: U.S.-Airbnb-Open-Data

https://www.openml.org/d/43369

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Kritik Seth

### Context
Since its inception in 2008, Airbnb has disrupted the traditional hospitality industry as more travellers decide to use Airbnb as their primary means of accommodation. Airbnb offers travellers a more unique and personalized way of accommodation and experience.

### Content
This dataset has columns describing features such as host id, hostname, listing id, listing name, latitude and longitude of listing, the neighbourhood, price, room type, minimum number of nights, number of reviews, last review date, reviews per month, availability, host listings and city.

### Acknowledgements
This dataset is a compilation of multiple datasets found on Inside Airbnb.

### Inspiration
* Can we predict the price of each house in different regions? 
* Can we describe a region using the names of listings in that region? 
* What can we learn about different regions from the data? 
* Based on different factors is it possible to recommend a title to the host for his/her listing? 
* Can we estimate the popularity of a listing based on given features?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43369) of an [OpenML dataset](https://www.openml.org/d/43369). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43369/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43369/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43369/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

